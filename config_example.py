class Config(object):
    DEBUG = False
    TESTING = False

    KEYCLOAK_CLIENT_ID = 'CLIENT-ID_DEFINE-IN-KEYCLOAK'
    KEYCLOAK_CLIENT_SECRET = 'CLIENT-SECRET_DEFINE-IN-KEYCLOAK'
    KEYCLOAK_CALLBACK_URL = 'https://SURCOUCHE-ERP.BASE.URL/callback'
    KEYCLOAK_METADATA_URL = 'https://KEYCLOAK.BASE.URL/auth/realms/REALM-NAME/.well-known/openid-configuration'

    ERP_APIKEY   = 'YOUR_PRODUCTION_ERP_APIKEY'
    ERP_BASEURL  = 'https://YOUR_PRODUCTION.ERP.BASEURL'


class ProductionConfig(Config):
    pass

class DevelopmentConfig(Config):
    DEBUG = True

    ERP_APIKEY   = 'YOUR_PREPROD_ERP_APIKEY'
    ERP_BASEURL  = 'https://YOUR_PREPROD.ERP.BASEURL'

class TestingConfig(Config):
    TESTING = True
