# Flask-WTF is an integration of WTForms in Flask
# https://flask-wtf.readthedocs.io/
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired

# WTForms is forms validation and rendering lib for Python web development
# https://wtforms.readthedocs.io/
# (Integreted in Flask-WTF)
from wtforms import StringField, IntegerField, SelectField, TextAreaField, BooleanField 
from wtforms.validators import DataRequired, ValidationError, URL


class addProductForm(FlaskForm):
    # An user can have an existing barcode for a product, or not
    # in the second case, it can be because
    # - the supplier didn't have a registered barcode
    # - or the product is to scale, and the barcode would be dynamic 
    barcodeType = SelectField(
        'Type de code-barre',
        choices=[
            ('existing', 'Code-barre existant'),
            ('non-existing', 'Code-barre inexistant'),
            ('to-scale', 'Produit à peser')
        ]
    )
    barcode = IntegerField('Code-barre', validators=[DataRequired()])
    
    # By default, we disabling edit of ref and completeName for calculate them
    ref = StringField('Référence', render_kw={'disabled':'true'}, validators=[DataRequired()])
    completeName =  StringField('Nom complet', render_kw={'disabled':'true'}, validators=[DataRequired()])
    name = StringField('Libellé', validators=[DataRequired()])
    brand = StringField('Marque')

    # Weight can be a real weight or a Volume
    weightType = SelectField(
        '',
        choices=[
            ('weight', 'Poids (g)'),
            ('volume', 'Volume (ml)')
        ]
    )
    weight = IntegerField('')

    stateSell = SelectField(
        "État (Vente)",
        choices=[
            (1, 'En Vente'),
            (0, 'Hors Vente')
        ]
    )
    stateBuy = SelectField(
        "État (Achat)",
        choices=[
            (1, 'En Achat'),
            (0, 'Hors Achat')
        ]
    )

    desc = TextAreaField('Description')
    note = TextAreaField('Note')
    
    category = SelectField("Catégorie", choices=[])
    subCategory = SelectField("Sous-Catégorie", choices=[])

    bio = BooleanField("Bio")
    mealVoucher = BooleanField("Tickets Restaurant")
    weeklyInventory = BooleanField("Inventaire hebdomadaire")

    # tags

    supplier = SelectField('Fournisseur', choices=[])
    priceSell = IntegerField("Prix de vente", validators=[DataRequired()])
    tva = SelectField(
        "Taux TVA",
        choices=[
            (0, 0),
            (2.1, 2.1),
            (5.5, 5.5),
            (10, 10),
            (20, 20)
        ],
        default=20
    )

    accountancyCodeSell = SelectField(
        "Code comptable (vente)",
        render_kw={'disabled':'true'},
        choices=[]
    )


    image = FileField('image', validators=[
        FileAllowed(
            ('jpg', 'jpe', 'jpeg', 'png'),
            'Uniquement un format jpg ou png'
        )
    ])
    imageURL = StringField('URL de l\'image', render_kw={'hidden':'true'})


class importProductForm(FlaskForm):
    csvFile = FileField('Fichier csv', validators=[
            FileAllowed(['csv'], 'Uniquement un format csv'),
            FileRequired()
        ]
    )