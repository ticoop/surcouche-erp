class Config(object):
    DEBUG = False
    TESTING = False

    KEYCLOAK_CLIENT_ID = 'TiCoop-SurcoucheERP'
    KEYCLOAK_CLIENT_SECRET = '48fbfb35-7184-40bc-8c9e-059d551f2d70'
    KEYCLOAK_CALLBACK_URL = 'https://product.ticoop.fr/callback'
    KEYCLOAK_METADATA_URL = 'https://my.ticoop.fr/auth/realms/ticoop/.well-known/openid-configuration'

    ERP_APIKEY   = 'uXg0qqNMlZ810Kf4P1sa35jLR0EsDq3S'
    ERP_BASEURL  = 'https://erp.ticoop.fr'

class ProductionConfig(Config):
    pass

class DevelopmentConfig(Config):
    DEBUG = True

    ERP_APIKEY   = '1f6352c7e06d0cab63fd8ead2e37196d7c82ed9d'
    ERP_BASEURL  = 'https://erp-form.ticoop.fr'

class TestingConfig(Config):
    TESTING = True
