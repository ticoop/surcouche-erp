class Config(object):
    DEBUG = False
    TESTING = False

    OIDC_ID_TOKEN_COOKIE_SECURE: False
    OIDC_REQUIRE_VERIFIED_EMAIL: False
    OIDC_USER_INFO_ENABLED: True
    OIDC_OPENID_REALM: 'ticoop'
    OIDC_SCOPES: ['openid', 'email', 'profile']
    OIDC_INTROSPECTION_AUTH_METHOD: 'client_secret_post'

    ERP_APIKEY   = 'client_secrets.json'
    ERP_BASEURL  = 'https://erp.ticoop.fr'

class ProductionConfig(Config):
    pass

class DevelopmentConfig(Config):
    DEBUG = True

    ERP_APIKEY   = '1f6352c7e06d0cab63fd8ead2e37196d7c82ed9d'
    ERP_BASEURL  = 'https://erp-form.ticoop.fr'

class TestingConfig(Config):
    TESTING = True
