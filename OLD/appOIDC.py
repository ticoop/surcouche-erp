"""
Python Flask WebApp Keycloak integration example


"""
import json
from os import environ as env
from werkzeug.exceptions import HTTPException
from werkzeug.utils import secure_filename

from flask import Flask, jsonify, redirect, render_template, session, url_for, flash, g
from flask_oidc import OpenIDConnect
from six.moves.urllib.parse import urlencode
from forms import addProductForm

from erp import Dolibarr
import constants
import requests

import logging

app = Flask(__name__, static_url_path='/public', static_folder='./public')
# logging.basicConfig(filename='record.log', level=logging.CRITICAL, format=f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')
logging.basicConfig(filename='record.log', level=logging.CRITICAL, format=f'%(levelname)s : %(message)s')

if app.config["ENV"] == "production":
    app.config.from_object("config.ProductionConfig")
else:
    app.config.from_object("config.DevelopmentConfig")

app.secret_key = constants.SECRET_KEY
app.debug = True

oidc = OpenIDConnect(app)

@app.errorhandler(Exception)
def handle_auth_error(ex):
    response = jsonify(message=str(ex))
    response.status_code = (ex.code if isinstance(ex, HTTPException) else 500)
    return response


keycloak = oauth.register(
    'keycloak',
    client_id=app.config["KEYCLOAK_CLIENT_ID"],
    client_secret=app.config["KEYCLOAK_CLIENT_SECRET"],
    server_metadata_url=app.config["KEYCLOAK_METADATA_URL"],
    client_kwargs={
        'scope': 'openid profile email roles',
    }
)

erp = Dolibarr(app.config["ERP_BASEURL"], app.config["ERP_APIKEY"])


# Controllers API
@app.route('/')
def home():
    return render_template('home.html.jinja')

# @app.route('/callback')
# def callback_handling():
#     token = keycloak.authorize_access_token()
#     userinfo = keycloak.parse_id_token(token)
    
#     for key in userinfo.keys():
#         app.logger.critical(key + " : " + str(userinfo[key]))

#     session[constants.JWT_PAYLOAD] = userinfo
#     session[constants.PROFILE_KEY] = {
#         'user_id': userinfo['sub'],
#         'name': userinfo['name']
#     }
#     return redirect('/addProduct')

# @app.route('/login')
# def login():
#     return keycloak.authorize_redirect(redirect_uri=app.config["KEYCLOAK_CALLBACK_URL"])

# @app.route('/logout')
# def logout():
#     session.clear()
#     params = {'redirect_uri': url_for('home', _external=True)}
#     return redirect(keycloak.server_metadata['end_session_endpoint']+'?' + urlencode(params))

@app.route('/dashboard')
@oidc.require_login
def dashboard():

    info = oidc.user_getinfo(['preferred_username', 'email', 'sub'])

    username = info.get('preferred_username')
    email = info.get('email')
    user_id = info.get('sub')

    if user_id in oidc.credentials_store:
        try:
            from oauth2client.client import OAuth2Credentials
            access_token = OAuth2Credentials.from_json(oidc.credentials_store[user_id]).access_token
            print('access_token=<%s>' % access_token)
            headers = {'Authorization': 'Bearer %s' % (access_token)}

        except:
            print("Could not access greeting-service")


    return ("""%s your email is %s and your user_id is %s!
               <ul>
                 <li><a href="/">Home</a></li>
                 <li><a href="https://my.ticoop.fr/auth/realms/ticoop/account?referrer=TiCoop-SurcoucheERP&referrer_uri=https://product.ticoop.fr/dashboard&">Account</a></li>
                </ul>""" %
            (email, user_id))


    # return render_template('dashboard.html.jinja',
    #                        userinfo=session[constants.PROFILE_KEY],
    #                        userinfo_pretty=json.dumps(session[constants.JWT_PAYLOAD], indent=4))


@app.route('/api', methods=['POST'])
@oidc.accept_token(require_token=True, scopes_required=['openid'])
def hello_api():
    """OAuth 2.0 protected API endpoint accessible via AccessToken"""

    return json.dumps({'hello': 'Welcome %s' % g.oidc_token_info['sub']})


# @app.route('/addProduct', methods=['GET', 'POST'])
# @requires_auth
# def addProduct():
#     form = addProductForm()
#     # form.barcode.data = 3068110702235
#     # form.barcode.data = 0123456789012
#     # form.barcode.data = 2100000000000

#     if form.validate_on_submit():
#         # To test
#         image = form.image.data
#         filename = secure_filename(image.filename)
#         image.save(os.path.join(
#             app.instance_path, 'images', filename
#         ))

#         flash('Produit ajouté : ' + form.name.data, 'success alert-success')
#         return redirect(url_for('addProduct'))
    
#     elif form.is_submitted():
#         flash('Erreur lors de la création du produit ', 'error alert-danger alert-dismissible fade show')

#     return render_template('addProduct.html.jinja', form=form)


# @app.route('/getSupplier')
# @requires_auth
# def getSupplier():
#     return jsonify(erp.getSupplier())

# @app.route('/getIdNewProduct/<filter>')
# @requires_auth
# def getIdNewProduct(filter):
#     return jsonify(erp.getIdNewProduct(filter))    

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=env.get('PORT', 5000))