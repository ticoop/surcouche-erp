class Product():
    def __init__(self, id):
        self.id                     = -1
        self.ref                    = -1
        self.label                  = -1
        self.desc                   = -1
        self.barcode                = -1
        self.bio                    = -1
        self.toWeight               = -1
        self.weight                 = -1
        self.weightUnit             = -1
        self.mealVoucher            = -1
        self.weeklyInventory        = -1
        self.category               = -1
        self.subCategory            = -1
        self.brand                  = -1
        self.statusSell             = -1
        self.statusBuy              = -1
        self.accountancyCodeBuy     = -1
        self.accountancyCodeSell    = -1
        self.stock                  = -1

    def errorOnEmptyValue(self, value):
        if value is None or len(str(value)) == 0:
            raise Exception("Value must be not empty")

    def checkIsEAN13(self, barcode):
        # Check if parameter is an Integer
        self.checkIsInt(barcode, "Barcode")

        barcode = str(barcode)
        barcodeLenght = len(barcode)

        # Check if barcode have between 12 and 13 digits
        if barcodeLenght < 12 or barcodeLenght > 13:
            raise Exception("Barcode must have 12 or 13 digits :" + barcode)

        # Get the check digit
        checkDigit = self.ean13Checksum(barcode)
        # If barcode has it already -> check is correct
        if barcodeLenght == 13 and barcode[12:13] != checkDigit:
            util.logWarning("The checksum of barcode " + barcode + " is wrong. Fix to " + checkDigit)

        # In all case return the barcode with the checkDigit calculate here
        return barcode[:12] + checkDigit

    def ean13Checksum(self, barcode):
        weight = [1,3]*6 # -> [1, 3, 1, 3, 1, 3, ...]
        magic = 10
        sum = 0
        
        # checksum based on first 12 digits.
        for i in range(12):
            # *1 if digit is in oven position, *3 if is in even position
            sum = sum + int(barcode[i]) * weight[i] 

        # Substract 10 by remainder of sum % 10
        checkDigit = magic - (sum % magic)
        # If checkDigit == 10 -> 0
        if checkDigit == magic:
            return '0'
        
        return str(checkDigit)

    def checkIsInt(self, value, name):
        try:
            int(value)
        except ValueError as error:
            raise Exception(name + " must be an Integer :" + value)

    def checkIsFloat(self, value, name):
        try:
            float(value)
        except ValueError as error:
            raise Exception(name + " must be a Float :" + value)

    def getId(self):
        return self.id

    def setId(self, value):
        self.id = value
    
    def getRef(self):
        return self.ref

    def setRef(self, value):
        self.ref = value

    def getLabel(self):
        return self.label

    def setLabel(self, value):
        self.label = value

    def getDescription(self):
        return self.desc

    def setDescription(self, value):
        self.desc = value

    def getBarcode(self):
        return self.barcode

    def setBarcode(self, value):
        self.barcode = value

    def getIsBio(self):
        return self.bio

    def setIsBio(self, value):
        self.bio = value

    def getIsToWeight(self):
        return self.toWeight

    def setIsToWeigth(self, value):
        self.toWeight = value

    def getWeight(self):
        return self.weight

    def setWeight(self, value):
        self.weight = value

    def getWeightUnit(self):
        return self.weightUnit

    def setWeightUnit(self, value):
        self.weightUnit = value

    def getIsMealVoucher(self):
        return self.mealVoucher

    def setIsMealVoucher(self, value):
        self.mealVoucher = value

    def getIsWeeklyInventory(self):
        return self.weeklyInventory

    def setIsWeeklyInventory(self, value):
        self.weeklyInventory = value

    def getCategory(self):
        return self.category

    def setCategory(self, value):
        self.category = value

    def getSubCategory(self):
        return self.subCategory

    def setSubCategory(self, value):
        self.subCategory = value

    def getBrand(self):
        return self.brand

    def setBrand(self, value):
        self.brand = value

    def getIsStatusSell(self):
        return self.statusSell

    def setIsStatusSell(self, value):
        self.statusSell = value

    def getIsStatusBuy(self):
        return self.statusBuy

    def setIsStatusBuy(self, value):
        self.statusBuy = value

    def getAccountancyCodeSell(self):
        return self.accountancyCodeSell

    def setAccountancyCodeSell(self, value):
        self.accountancyCodeSell = value
    
    def getAccountancyCodeBuy(self):
        return self.accountancyCodeBuy

    def setAccountancyCodeBuy(self, value):
        self.accountancyCodeBuy = value

    def getStock(self):
        return self.stock

    def setStock(self, value):
        self.stock = value


class DolibarrProduct(Product):
    def __init__(self, product_id):
        try:
            data = erp.getProductById(product_id)

        except Exception as error:
            raise Exception("ERREUR : lors de la récupération des données du produit " + repr(error))

        self.id                     = data['id']
        self.ref                    = data['ref']
        self.label                  = data['label']
        self.desc                   = data['desc']
        self.barcode                = data['barcode']
        self.bio                    = data['array_options']['options_bio']
        self.toWeight               = data['array_options']['options_toweight']
        self.weight                 = data['weight']
        self.weightUnit             = data['weight_units']
        self.mealVoucher            = data['array_options']['options_meal_voucher_ok']
        self.weeklyInventory        = data['array_options']['options_weeky_inventory']
        self.category               = data['array_options']['options_categorie']
        self.subCategory            = data['array_options']['options_scategorie']
        self.brand                  = data['array_options']['options_marque']
        self.statusSell             = data['status']
        self.statusBuy              = data['status_buy']
        self.accountancyCodeBuy     = data['accountancy_code_buy']
        self.accountancyCodeSell    = data['accountancy_code_sell']
        self.stock                  = data['stock_reel']


    def setId(self, value):
        util.logWarning("Tentative de modifier l'id refusée")
    
    def setRef(self, value):
        self.errorOnEmptyValue(value)
        self.ref = value

    def setLabel(self, value):
        self.errorOnEmptyValue(value)
        self.label = value

    def setBarcode(self, value):
        barcode = self.checkIsEAN13(value)

        productList = erp.getProductByBarcode(barcode)
        if len(productList) > 1:
            raise Exception(
                "Plusieurs produits ont le même code-barre " +
                barcode
            )

        elif len(productList) == 1 and productList[0]['id'] != self.id:
            raise Exception(
                "Le produit " +
                productList[0]['id'] +
                " a déjà ce code-barre " +
                barcode
            )

        self.barcode = barcode

    def getIsBio(self):
        return self.bio == "1"

    def setIsBio(self, value):
        if value:
            self.bio = "1"
        else:
            self.bio = ""

    def getIsToWeight(self):
        return self.toWeight == "1"

    def setIsToWeigth(self, value):
        if value:
            self.toWeight = "1"
        else:
            self.toWeight = ""

    def setWeight(self, value):
        self.checkIsFloat(value, "Weight")
        if float(value) < 0:
            raise Exception("Weight must be >= 0")

        value = str(value)
        self.weight = value

    def getWeightUnit(self):
        if self.weightUnit == '1':
            return 'tonnes'
        elif self.weightUnit == '2':
            return 'Kg'
        elif self.weightUnit == '3':
            return 'g'
        elif self.weightUnit == '4':
            return 'mg'
        else:
            return 'unknow'

    def setWeightUnit(self, value):
        self.checkIsInt(value, "Weight unit")
        if int(value) == 0:
            self.weightUnit = ''
        if int(value) >= 1 and int(value) <= 4:
            self.weightUnit = value
        else:
            raise Exception(
                "Weight unit must be an Integer between 0 and 4 " +
                "0 for None " +
                "1 for tonnes " +
                "2 for Kg " +
                "3 for g " +
                "4 for mg"
            )

    def getIsMealVoucher(self):
        return self.mealVoucher == "1"

    def setIsMealVoucher(self, value):
        self.mealVoucher = value
        if value:
            self.mealVoucher = "1"
        else:
            self.mealVoucher = ""

    def getIsWeeklyInventory(self):
        return self.weeklyInventory == "1"

    def setIsWeeklyInventory(self, value):
        if value:
            self.weeklyInventory = "1"
        else:
            self.weeklyInventory = ""

    def getCategory(self):
        return self.category

    def setCategory(self, value):
        self.checkIsInt(value, "Category")
        if int(value) < 1 or int(value) > 9:
            raise Exception(
                "Category must be an Integer between 1 and 9"
            )
        value = str(value)
        self.category = value

    def getSubCategory(self):
        return self.subCategory

    def setSubCategory(self, value):
        self.checkIsInt(value, "SubCategory")
        if int(value) < 1 and int(value) > 150:
            raise Exception(
                "SubCategory must be an Integer between 1 and 150"
            )
        value = str(value)
        self.subCategory = value

    def setBrand(self, value):
        self.brand = value

    def getIsStatusSell(self):
        return self.statusSell == "1"
        
    def setIsStatusSell(self, value):
        if value:
            self.statusSell = "1"
        else:
            self.statusSell = ""

    def getIsStatusBuy(self):
        return self.statusBuy == "1"

    def setIsStatusBuy(self, value):
        if value:
            self.statusBuy = "1"
        else:
            self.statusBuy = ""

    def getAccountancyCodeSell(self):
        return self.accountancyCodeSell

    def setAccountancyCodeSell(self, value):
        self.checkIsInt(value, "Accountancy Code Sell")
        value = str(value)
        if len(value) != 8:
            raise Exception(
                "Accountancy Code Sell must be an Integer with 8 digits"
            )

        if not value.startswith('707'):
            raise Exception(
                "Accountancy Code Sell must start by 707"
            )

        self.accountancyCodeSell = value
    
    def getAccountancyCodeBuy(self):
        return self.accountancyCodeBuy

    def setAccountancyCodeBuy(self, value):
        self.checkIsInt(value, "Accountancy Code Buy")
        value = str(value)
        if len(value) != 8:
            raise Exception(
                "Accountancy Code Buy must be an Integer with 8 digits"
            )

        if not value.startswith('606'):
            raise Exception(
                "Accountancy Code Buy must start by 606"
            )

        self.accountancyCodeBuy = value

    def setStock(self, value):
        self.checkIsFloat(value, "Stock")
        if float(value) < 0:
            raise Exception(
                "Stock must be >= 0"
            )
        value = str(value)
        self.stock = value
