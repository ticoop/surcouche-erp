# Version 1.0 - 2020-05-17 - Cyril CARGOU - Script pour mettre à jour des produits en masse
# Version 2.0 - 2022-01-12 - Maxime Collin - Adaptation pour les Tickets Resto
# Version 3.0 - 2022-01-31 - Maxime Collin - Adaptation pour prendre en compte n'importe quel champ
# 
# Ce script prend en entrée un fichier csv pour mettre à jour des produits dans l'ERP
# Il ne mettra à jour que les colonnes présentes dans le fichier
#  
# Les entêtes des colonnes doivent être la clé utilisée dans l'ERP (ex : barcode, accountancy_code_sell, ...)
# La colonne id doit être présente
# 
# EXEMPLE : 
# id;barcode;options_toweight
# 42;123456798123;1
# 
# L'ordre des colonnes n'a pas d'importance
# Les valeurs OUI et NON (insensibles à la casse) seront converties en 0 ou 1
# 
# USAGE :
# python updateProduit.py
#   --file=/path/to/inputFile.csv    (Optionnel) Le chemin du fichier csv en input (Par défaut ./updateProduits.csv)
#   --backup=/path/to/backupFile.csv (Optionnel) Le chemin du fichier de backup (Par défaut ./backupProduits.csv)
#   --delimiter=,                    (Optionnel) Le délimiteur de champ csv (Par défaut ';')
#   --whatIf                         (Optionnel) Ne commite pas les données en bdd (Par défaut elles sont commitées)
#   --prod                           (Optionnel) Travaille sur la prod (Par défaut en formation)
# 
# CORRESPONDANCE CHAMPS DOLIBARR :
#   Code-barre -> barcode
#   État (vente) -> status
#   État (achat) -> status_buy
#   Libellé -> label
#   Réf. -> ref
#   Bio -> options_bio
#   À peser -> options_toweight
#   Tickets Resto -> options_meal_voucher_ok
#   Code comptable (vente) -> accountancy_code_sell
#   Code comptable (achat) -> accountancy_code_buy
#  

##############################################################################################################################################
############################################################### IMPORT MODULES ###############################################################
##############################################################################################################################################

import os   # Pour la gestion des chemins
import csv  # Pour l'export en csv
import re   # Pour le contrôle

import _credential
from TiCoopLib.Dolibarr import erp
from TiCoopLib.Util import util

##############################################################################################################################################
############################################################## DEFINE FUNCTIONS ##############################################################
##############################################################################################################################################
# -----------------------------------------------------------------------------------------------
# --------------------------------------- checkHeaders() ----------------------------------------
# -----------------------------------------------------------------------------------------------
# 
# DESCRIPTION :
#   Cette fonction va vérifier que les headers passés en paramètres
#   correspondent à des champs dans l'ERP
# 
# PARAMÈTRES :
#   keyList:list = la liste des headers
# 
# RETURN :
#   True si c'est ok, False sinon
# 
# USAGE :
#   checkHeaders(['id', 'barcode', 'options_toweight'])
# 
def checkHeaders(keyList):
    # By default, we consider that headers are OK
    result = True
    # We get 1 product from ERP
    # product = erp.getProduct(limit="1")[0]

    # Loop on headers
    for key in keyList:
        # if the key aren't in product
        # if key not in product and key not in product['array_options']:
        if key not in erpFields.fieldMethod:
            result = False
            util.logError("ERREUR : L'entête " + key + " ne figure pas dans les champs de l'ERP")

    return result

class SetERPDataFromCSV():

    def __init__(self, row, idIndex, headerList):
        self.row = row
        self.idIndex = idIndex
        self.headerList = headerList

        try:
            self.getId()
            self.product_data = DolibarrProduct(erp.getProductById(self.product_id))

        except Exception as error:
            raise Exception("ERREUR : lors de la récupération des données du produit " + repr(error))
        
        for index in range(0,len(self.row)):
            # Get the data field name and value
            header = self.headerList[index]
            newValue = self.row[index]

            try:
                setValueMethod = getattr(self, erpFields.fieldMethod[header])
                setValueMethod(header, newValue)

            except Exception as error:
                raise error

        self.checkValidData()


    def checkValidData(self):
        self.product_data.checkWeight() # Si à peser, poids non null
        self.product_data.check.AccountancyCode() # Même code comptable (vente et achat)
        self.product_data.checkCategory() # Que la sous-catégorie soit bien dans la catégorie


    def getId(self):
        self.product_id = self.row[self.idIndex]
        self.__getIntValue(self.product_id, "L'Id")
        self.row.pop(self.idIndex)

    def setId(self, fieldName, newValue):
        util.logWarning("Tentative de modifier l'id refusée")

    def setRef(self, fieldName, newValue):
        self.product_data[fieldName] = newValue

    def setLabel(self, fieldName, newValue):
        self.product_data[fieldName] = newValue

    def setDesc(self, fieldName, newValue):
        self.product_data[fieldName] = newValue

    def setBarcode(self, fieldName, newValue):
        barcode = self.__getIntValue(newValue, "Le code-barre")

        if len(barcode) < 12 or len(barcode) > 13:
            raise Exception(
                "Le code-barre " +
                barcode +
                " n'a pas 12 ou 13 chiffres"
            )

        productList = erp.getProductByBarcode(barcode)
        if len(productList) > 1:
            raise Exception(
                "Plusieurs produits ont le même code-barre " +
                barcode
            )

        elif len(productList) == 1 and productList[0]['id'] != self.product_data['id']:
            raise Exception(
                "Le produit " +
                productList[0]['id'] +
                " a déjà ce code-barre " +
                barcode
            )


        self.product_data[fieldName] = barcode

    def setBio(self, fieldName, newValue):
        isTrue = self.__getBooleanValue(newValue)
        self.product_data[fieldName] = isTrue

    def setToWeigth(self, fieldName, newValue):
        isTrue = self.__getBooleanValue(newValue)
        self.product_data[fieldName] = isTrue

    def setMealVoucher(self, fieldName, newValue):
        isTrue = self.__getBooleanValue(newValue)
        self.product_data[fieldName] = isTrue

    def setWeeklyInventory(self, fieldName, newValue):
        isTrue = self.__getBooleanValue(newValue)
        self.product_data[fieldName] = isTrue

    def setCategory(self, fieldName, newValue):
        intValue = self.__getIntValue(newValue, "La catégorie")
        self.product_data[fieldName] = intValue

    def setSubCategory(self, fieldName, newValue):
        intValue = self.__getIntValue(newValue, "La sous-catégorie")
        self.product_data[fieldName] = intValue

    def setBrand(self, fieldName, newValue):
        self.product_data[fieldName] = newValue

    def setStatusSell(self, fieldName, newValue):
        isTrue = self.__getBooleanValue(newValue)
        self.product_data[fieldName] = isTrue

    def setStatusBuy(self, fieldName, newValue):
        isTrue = self.__getBooleanValue(newValue)
        self.product_data[fieldName] = isTrue

    def setAccountancyCodeSell(self, fieldName, newValue):
        accountancy_code_sell = self.__getIntValue(newValue, "Le code comptable de vente")

        if len(accountancy_code_sell) != 8:
            raise Exception(
                "Le code comptable de vente " +
                accountancy_code_sell +
                " n'a pas 8 chiffres"
            )

        self.product_data[fieldName] = accountancy_code_sell

    def setAccountancyCodeBuy(self, fieldName, newValue):
        accountancy_code_buy = self.__getIntValue(newValue, "Le code comptable d'achat")

        if len(accountancy_code_buy) != 8:
            raise Exception(
                "Le code comptable d'achat " +
                accountancy_code_buy +
                " n'a pas 8 chiffres"
            )

        self.product_data[fieldName] = accountancy_code_buy

    def setStock(self, fieldName, newValue):
        print("setStock to define")
        # floatValue = self.__getFloatValue(newValue, "Le stock")
        # self.product_data[fieldName] = floatValue

    def setWeight(self, fieldName, newValue):
        floatValue = self.__getFloatValue(newValue, "Le poids")
        self.product_data[fieldName] = floatValue

    def setWeightUnit(self, fieldName, newValue):
        intValue = self.__getIntValue(newValue, "L'unité de poids")
        self.product_data[fieldName] = intValue

    def __getIntValue(self, value, name):
        value = str(value)
        if not re.match('^[0-9]+$', value): 
            raise Exception(name + " " + value + " n'est pas un nombre entier")

        return value

    def __getFloatValue(self, value, name):
        value = str(value)
        if not re.match('^[0-9]+[,.]?[0-9]*$', value): 
            raise Exception(name + " " + value + " n'est pas un nombre")

        return value        

    def __getBooleanValue(self, value):
        value = str(value).upper()
        
        falseValue = ['', 'NON', 'N', 'FALSE', 'F', '0']
        trueValue = ['OUI', 'O', 'TRUE', 'T', '1']
        if value in falseValue:
            return None

        elif value in trueValue:
            return '1'

        else:
            raise Exception(
                "La valeur " + value +
                " n'est pas booléenne : " +
                ' '.join(trueValue) +
                ' '.join(falseValue) + " ''"
            )


##############################################################################################################################################
#################################################################### MAIN ####################################################################
##############################################################################################################################################

util.logOpen()

# ------------------------------ GET PARAMETERS ------------------------------

# Chemin du fichier à importer
inputFilePath = util.getScriptParameter('file=','updateProduits.csv')
util.logInfo("Fichier à importer : Argument --file=" + str(inputFilePath))

# Chemin du fichier de backup
backupfilePath = util.getScriptParameter('backup=','backupProduits.csv')
util.logInfo("Fichier backup : Argument --backup=" + str(backupfilePath))

# Délimiteur de champ
delimiter = util.getScriptParameter('delimiter=',';')
util.logInfo("délimiteur : Argument --delimiter=" + str(delimiter))

# Données commitées ? Oui par défaut
isTest = util.getScriptParameter('whatIf',False)
util.logInfo("Indique si c'est un test : Argument --whatIf => " + str(isTest))

# Prod ? Non par défaut
isProd = util.getScriptParameter('prod',False)
util.logInfo("Indique si c'est la prod : Argument --prod => " + str(isProd))

# ----------------------- CHECK PARAMETERS ------------------------

# Check input file path
if not os.path.isfile(inputFilePath):
    util.logCloseAndExitOnError("Le fichier d'import n'exsite pas")


# -------------- SET CREDENTIAL -------------------
if isProd:
    erp.apiKey  = _credential.erp_apiKeyProduction
    erp.baseURL = _credential.erp_baseUrlProduction
    erpFields = DolibarrFields()

else:
    erp.apiKey  = _credential.erp_apiKeyFormation
    erp.baseURL = _credential.erp_baseUrlFormation
    erpFields = DolibarrFields('form')


# ----------------------------------------------------------------- PROCESS ------------------------------------------------------------------
try:
   
    # Export de l'ensemble des articles en ventes
    util.logInfo("Lecture du fichier csv : " + inputFilePath) 

    line_count = 0
    # Open the 2 files : input and backup
    with open(inputFilePath, 'r', encoding='ISO8859') as input_file, open(backupfilePath, 'w', encoding='ISO8859') as backup_file:
        # Loop on input file
        for line in input_file:
            try:
                # Split the line with csv delimiter
                row = line.rstrip('\r\n').split(delimiter)

                # ------------------------------------- ON HEADER -------------------------------------
                if line_count == 0 :
                    # Get the headers
                    keyList = [key.lower() for key in row]

                    # Just check if headers is OK
                    if checkHeaders(keyList) == False:
                        util.logCloseAndExitOnError("Les entêtes ne sont pas bonnes")
                    
                    # Attempt to get the index of id headers
                    # And remove it from the list
                    try:
                        idIDX = keyList.index('id')
                        keyList.pop(idIDX)

                    except ValueError as error:
                        util.logCloseAndExitOnError("La colonne id n'est pas présente dans le fichier")

                    # Write the headers in backup file
                    backup_file.write(line)

                    # New line
                    line_count += 1

                # -------------------------------------------------------- ON CONTENT --------------------------------------------------------
                else:
                    # -------------------------------------------------------- CHECK ---------------------------------------------------------
                    # Check if we have the same number of column
                    # -> skip line else
                    if len(row) != len(keyList) +1:
                        util.logError("ERREUR : La ligne " + str(line_count+1) + " n'a pas le bon nombre de colonne -> Passage à la suivante")
                        continue

                    test = SetERPDataFromCSV(row, idIDX, keyList)

                    # ------------ GET PRODUCT DATA BY ID ------------
                    # Get the id of the product
                    # product_id = row[idIDX]
                    # row.pop(idIDX)
                    # util.logInfo("Traitement de l'id : " + product_id)

                    # # validation de données
                    # if not re.match('[0-9]+',product_id): 
                    #     raise Exception("ID n'est pas un nombre")

                    # # Get the data of the product on ERP
                    # try:
                    #     product_data = erp.getProductById(product_id)

                    # except Exception as error:
                    #     util.logError("ERREUR : "+repr(error))
                    #     continue

                    # # Add the id to backup data
                    # backup = str(product_id)


                    # # ------------------------ LOOP ON ROW'S COLUMNS ------------------------
                    # for index in range(0,len(row)):
                    #     # Get the data field name and value
                    #     dataKey = keyList[index]
                    #     dataNewValue = row[index]

                    #     # Parse the value
                    #     if dataNewValue.upper() == 'OUI':
                    #         dataNewValue = '1'

                    #     elif dataNewValue == '' or dataNewValue.upper() == 'NON':
                    #         dataNewValue = None

                    #     # Change the data value in product and store his old value for backup
                    #     if dataKey in product_data['array_options']:
                    #         dataOldValue = product_data['array_options'][dataKey]
                    #         product_data['array_options'][dataKey] = dataNewValue

                    #     else:
                    #         dataOldValue = product_data[dataKey]
                    #         product_data[dataKey] = dataNewValue

                    #     # Change None value to empty string for backup
                    #     if dataOldValue == None:
                    #         dataOldValue = ''

                    #     # Put the old value in backup
                    #     backup += ';' + str(dataOldValue)

                    # # WRITE THE LINE IN BACKUP FILE
                    # backup_file.write(backup + '\n')


                    # --------------------- UPDATE PRODUCT ----------------------
                    if not isTest :
                        response = erp.updateProductById(product_id,product_data)

                    line_count += 1
            except Exception as error:
                util.logError("ERREUR : "+repr(error))

        util.logInfo("Nombre de ligne traitée : " + str(line_count-1)) 

    util.logCloseAndExit()
except Exception as error:
    util.logCloseAndExitOnError(repr(error))
