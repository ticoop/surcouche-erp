class ERPFields():
    fieldMethod = []

class DolibarrFields(ERPFields):
    fieldMethod = {
        'id'                        : 'setId',
        'ref'                       : 'setRef',
        'label'                     : 'setLabel',
        'description'               : 'setDesc',
        'barcode'                   : 'setBarcode',
        'options_bio'               : 'setBio',
        'options_toweight'          : 'setToWeigth',
        'options_meal_voucher_ok'   : 'setMealVoucher',
        'options_weeky_inventory'   : 'setWeeklyInventory',
        'options_categorie'         : 'setCategory',
        'options_scategorie'        : 'setSubCategory',
        'options_marque'            : 'setBrand',
        'status'                    : 'setStatusSell',
        'status_buy'                : 'setStatusBuy',
        'accountancy_code_buy'      : 'setAccountancyCodeSell',
        'accountancy_code_sell'     : 'setAccountancyCodeBuy',
        'stock_reel'                : 'setStock',
        'weight'                    : 'setWeight',
        'weight_units'              : 'setWeightUnit'
    }

    def __init__(self, prod='prod'):
        if prod != 'prod':
            self.fieldMethod['options_sscategorie'] = self.fieldMethod.pop('options_scategorie')