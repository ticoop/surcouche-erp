#!/usr/bin/python
#
#Version 1.0 - 2020-03-25 - Cyril CARGOU - Script d'export des produits et création des étiquettes
#
#python3 ./script.py


#import du service client REST API
import requests
from requests.auth import HTTPBasicAuth
import json
#pour la gestion des accents
import unicodedata
#pour la gestion des regex
import re
#pour le filtre des articles modifiés
import datetime,time


class ERP:
    def getSupplier(self):
        raise NotImplementedError()

    def getIdNewProduct(self, filter):
        raise NotImplementedError()

class Dolibarr(ERP):

    def __init__(self, baseURL, apiKey):
        self.baseURL = baseURL
        self.apiKey = apiKey

        #Pour gagner du temps pour la récupération des noms des fournisseur associée à un produit
        self.cacheSupplier = None

        # La limite par défaut est 100 ce qui est souvent trop petit pour récupérer l'ensemble des résultats. Une limite à -1 est l'infini
        self.limitALL = 'limit=-1'


    #Fonction prive de requetes API
    def _apiGet(self, URI):
        return self._api(URI, 'GET')

    #Fonction prive de requetes API
    def _apiPost(self, URI,data):
        return self._api(URI, 'POST',data)

    #Fonction prive de requetes API
    def _apiPut(self, URI,data):
        return self._api(URI, 'PUT',json.dumps(data))

    #Fonction prive de requetes API
    def _api(self, URI,type='GET',data=None):
        headers = {'DOLAPIKEY': self.apiKey}
        url = self.baseURL  + URI

        if  type == 'GET':
            response = requests.get(url, headers=headers)
        elif type == 'POST':
            response = requests.post(url, headers=headers, data = data)
        elif type == 'PUT':
            headers['Accept'] = 'application/json'
            headers['Content-Type'] = 'application/json'
            response = requests.put(url, headers=headers, data = data)
        else:
            raise Exception('_api request type unknow : ' + type)
        #print(response)
        #print(response.headers)
        #print(response.text)
        #print(response.content)
        #print(response.encoding)
        #response.raise_for_status()
        if response.status_code == 404:#pas de data
            return []

        if response.status_code != 200:
            raise Exception('_api status_code : ' + str(response.status_code) + " URL : " + url)

        return response.json()

    #test si la connexion est OK
    def testConnexion(self):
        try:
            response = self._apiGet('/api/index.php/status')
            if response['success']['code'] != 200:
                return False
            return True
        except Exception:
            return False


    def getSupplierList(self):
        #récupération du fourniseur de ce stock
        url = "/api/index.php/thirdparties?sortfield=t.rowid&sortorder=DESC&" + self.limitALL + '&sqlfilters=t.fournisseur=1'
        supplierList = self._apiGet(url)

        return supplierList


    def getIdNewProduct(self, filter):
        refFilter = "&sqlfilters=t.ref%20like%20'" + filter + "%25'"
        url = "/api/index.php/products?sortfield=t.ref&sortorder=DESC&limit=1" + refFilter
        lastProduct = self._apiGet(url)

        if len(lastProduct) > 0:
            idNewProduct = int(lastProduct[0]["ref"][len(filter):]) + 1
            if idNewProduct < 10:
                return "00" + str(idNewProduct)
            elif idNewProduct < 100:
                return "0" + str(idNewProduct)
            else:
                return str(idNewProduct)
        
        return "000"
