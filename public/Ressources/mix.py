notToScale = [
    (
        1,
        'Boisson',
        (
            70760001,
            'BOISSONS NON ALCOLISEES',
            'BOI',
            (
                (69,'Boisson végétale'),
                (71,'Jus de fruits conditionnés'),
                (73,'Kombucha'),
                (74,'Mélange de jus de fruits conditionné'),
                (75,'Sirops conditionnés'),
                (77,'Sodas et softs')
            )
        ),
        (
            70760002,
            'BOISSONS ALCOLISEES',
            'ALC',
            (
                (66,'Apéritifs alcoolisés'),
                (67,'Bières artisanales'),
                (68,'Bières classiques'),
                (70,'Cidre'),
                (78,'Vin blanc bouteille'),
                (80,'Vin rosé bouteille'),
                (82,'Vin rouge bouteille')
            )
        )
    ),


    (
        2,
        'Droguerie & Hygiène',
        (
            70780001,
            "PRODUITS D'HYGIENE",
            'HYG',
            (
                (88,'Cosmétiques'),
                (94,'Hygiène bébé'),
                (95,'Outils hygiène'),
                (100,'Produits hygiène conditionnés')
            )
        ),
        (
            70780002,
            "PRODUITS D'ENTRETIEN",
            'ENT',
            (
                (97,'Produits d’entretien conditionnés')
            )
        ),
        (
            70780003,
            'DROGUERIE',
            'DRO',
            (
                (87,'Cartons et sachets réglementaires'),
                (89,'Divers droguerie'),
                (90,'Droguerie cuisine'),
                (93,'Fournitures'),
                (105,'Ustensiles cuisine')
            )
        ),
        (
            70780004,
            'SANTE',
            'SAN',
            (
                (96,'Pharmacie')
            )
        ),
        (
            70780011,
            'PRODUITS PÉRIODIQUES FÉMININ',
            'HYG',
            (
                (103,'Protections hygiéniques jetables'),
                (104,'Protections hygiéniques réutilisables')
            )
        ),
        (
            70780031,
            'GRAINES 10%',
            'JAR'
            ()
        )

    ),


    (
        3,
        'Épicerie Salée',
        (
            70740001,
            'EPICERIE SALEE',
            'SAL',
            (
                (1,'Aides culinaires conditionnées'),
                (3,'Algues séchées conditionnées'),
                (5,'Céréales diverses conditionnées'),
                (7,'Chips et biscuits apéritifs'),
                (8,'Condiments et sauces conditionnés'),
                (10,'Conserves de légumes'),
                (11,'Conserves de poisson'),
                (12,'Conserves de viande'),
                (13,'Épices'),
                (14,'Fruits à coque conditionnés'),
                (18,'Herbes séchées'),
                (19,'Huiles conditionnées'),
                (21,'Légumineuses conditionnées'),
                (24,'Mélanges épices'),
                (25,'Pâtes sèches conditionnées'),
                (27,'Plats préparés'),
                (28,'Riz conditionné'),
                (30,'Sauces tomates et dérivés'),
                (31,'Sels et Poivres'),
                (32,'Soupes préparées'),
                (33,'Spécialités végétales'),
                (34,'Tartinables salés'),
                (35,'Vinaigres conditionnés')
            )
        )
    ),


    (
        4,
        'Épicerie Sucrée',
        (
            70740002,
            'EPICERIE SUCREE',
            'SUC',
            (
                (38,'Aides à la pâtisserie'),
                (39,'Barres de céréales'),
                (40,'Biscuits conditionnés bio'),
                (41,'Biscuits conditionnés conventionnel'),
                (43,'Cafés conditionnés'),
                (45,'Céréales petit déjeuner conditionnées'),
                (50,'Compotes'),
                (53,'Confitures'),
                (54,'Farines'),
                (55,'Galettes de céréales & tartines'),
                (56,'Gâteaux'),
                (57,'Infusions'),
                (58,'Miel'),
                (60,'Sucre conditionné'),
                (63,'Thé conditionné')
            )
        ),
        (
            70740012,
            'CHOCOLATS / CONFISERIE TVA 20%',
            'SUC',
            (
                (47,'Chocolat dégustation'),
                (48,'Chocolat en poudre'),
                (51,'Confiseries conditionnées')
            )
        )
    ),


    (
        5,
        'Frais',
        (
            70730001,
            'VIANDES',
            'VIA',
            (
                (130,'Viande de bœuf')
            )
        ),
        (
            70730002,
            'VOLAILLE',
            'VOL',
            (
                (131,'Volaille')
            )
        ),
        (
            70730003,
            'CHARCUTERIE',
            'CHAR',
            (
                (115,'Charcuterie et salaisons')
            )
        ),
        (
            70750001,
            'PRODUITS FRAIS',
            'FRA',
            (
                (112,'Apéros frais'),
                (119,'Desserts frais'),
                (128,'Oeufs'),
                (129,'Pâtes à dérouler')
            )
        ),
        (
            70720002,
            'PRODUITS LAITIERS ET ASSIMILES',
            'FRA'
            (
                (114,'Beurre'),
                (116,'Crème'),
                (117,'Crèmerie Trahon Bihan'),
                (118,'Crèmes dessert'),
                (126,'Lait frais'),
                (127,'Lait longue conservation'),
                (132,'Yaourts bio'),
                (133,'Yaourts conventionnels') 
            )
        ),
        (
            70720001,
            'FROMAGE',
            'FRO',
            (
                (113,'Autres fromages'),
                (120,'Fromages à pâte dure'),
                (121,'Fromages à pâte molle'),
                (122,'Fromages à pâte persillée'),
                (123,'Fromages artisanaux locaux'),
                (124,'Fromages frais'),
                (125,'Fromages râpés')
            )
        ),
        (
            70770001,
            'POISSON',
            'POI',
            ()
        )

    ),


    (
        6,
        'Pain',
        (
            70740003,
            'PAIN',
            'PAI',
            (
                (107,'Pain Canevet'),
                (108,'Pain Babel'),
                (109,'Crêperie'),
                (110,'Pains conditionnés')
            )
        )
    ),

    (
        9,
        'Autre',
        (
            70700009,
            'AUTRES',
            'AUT',
            (
                (145,'Contenants')
            )
        )

    )
]

toScale = [
    (
        1,
        'Boisson',
        (
            70760001,
            'BOISSONS NON ALCOLISEES',
            'BOI',
            (
                (72,'Jus de fruits vrac'),
                (76,'Sirops vrac')
            )
        ),
        (
            70760002,
            'BOISSONS ALCOLISEES',
            'ALC',
            (
                (79,'Vin blanc vrac'),
                (81,'Vin rosé vrac'),
                (83,'Vin rouge vrac')
            )
        )
    ),


    (
        2,
        'Droguerie & Hygiène',
        (
            70780001,
            "PRODUITS D'HYGIENE",
            'HYG',
            (
                (101,'Produits hygiène liquide vrac'),
                (102,'Produits hygiène solide vrac')
            )
        ),
        (
            70780002,
            "PRODUITS D'ENTRETIEN",
            'ENT',
            (
                (98,'Produits d’entretien vrac')
            )
        ),
        (
            70780031,
            'GRAINES 10%',
            'JAR'
            ()
        )

    ),


    (
        3,
        'Épicerie Salée',
        (
            70790001,
            'VRAC SALE',
            'SAL',
            (
                (2,'Aides culinaires vrac'),
                (4,'Algues vrac'),
                (6,'Céréales diverses vrac'),
                (9,'Condiments et sauces vrac'),
                (15,'Fruits à coque vrac'),
                (16,'Fruits séchés vrac'),
                (17,'Graines vrac'),
                (20,'Huiles vrac'),
                (22,'Légumineuses vrac'),
                (23,'Mélange fruits à coque vrac'),
                (26,'Pâtes sèches vrac'),
                (29,'Riz vrac'),
                (36,'Vinaigres vrac')
            )
        )
    ),


    (
        4,
        'Épicerie Sucrée',
        (
            70790002,
            'VRAC SUCRE',
            'SUC',
            (
                (42,'Biscuits vrac'),
                (44,'Cafés vrac'),
                (46,'Céréales petit déjeuner vrac'),
                (59,'Sirops vrac'),
                (61,'Sucre vrac'),
                (62,'Tartinables sucrés'),
                (64,'Thé vrac')
            )
        ),
        (
            70790021,
            'VRAC SUCRE 20%',
            'SUC',
            ()
        ),
        (
            70740012,
            'CHOCOLATS / CONFISERIE TVA 20%',
            'SUC',
            (
                (49,'Chocolat pâtissier vrac'),
                (52,'Confiseries vrac')
            )
        )
    ),


    (
        5,
        'Frais',
        (
            70720003,
            'FROMAGE A LA COUPE',
            'FRO',
            (
                (113,'Autres fromages'),
                (120,'Fromages à pâte dure'),
                (121,'Fromages à pâte molle'),
                (122,'Fromages à pâte persillée'),
                (123,'Fromages artisanaux locaux'),
                (124,'Fromages frais'),
                (125,'Fromages râpés')
            )
        )
    ),


    (
        7,
        'Legumes',
        (
            70710001,
            'LEGUMES',
            'LEG',
            (
                (134,'Salade'),
                (135,'Tubercules'),
                (136,'Bulbes'),
                (137,'Choux'),
                (138,'Autre')
            )
        )
    ),


    (
        8,
        'Fruits',
        (
            70710002,
            'FRUITS',
            'FRU',
            (
                (139,'Pommes-Poires'),
                (140,'Agrumes'),
                (141,'Fruits Exotique'),
                (142,'Fruit Rouge'),
                (143,'Tomate-Avocat'),
                (144,'Autre')
            )
        )
    )
]