// Cette fonction va mettre les valeurs passées en paramètres dans les bons champs
// Peut être appelée sans paramètre avec pour résultat de réinitialiser les champs
function setInput(name, brand, weight, image_url)
{
    $('#name').val(name)
    $('#brand').val(brand)
    $('#weight').val(weight)
    $('#imageURL').val(image_url)
    $('#imageURL-preview').prop('src', image_url)

    // trigger the change event for populateCompleteName
    $('#name').trigger('change');
}

// Cette fonction va récupérer les infos d'un produit sur OpenFoodFacts.org à partir d'un code-barre
function getOpenFoodFactProductInfo()
{
    if (validateBarcode())
    {
        $.ajax
        ({
            type: "GET",
            url: "https://world.openfoodfacts.org/api/v0/product/" + $("#barcode").val() + ".json"
        })
        .done
        (
            function( resp )
            {
                if (resp.status == 1)
                {
                    setInput
                    (
                        resp.product.product_name_fr,
                        resp.product.brands,
                        resp.product.product_quantity,
                        resp.product.image_front_small_url
                    );
                } else
                {
                    invalidFeedback('#barcode', 'Désolé le code barre est introuvable');
                    setInput();
                }
            }
        );
    }
}   

// on button search click or on 'enter' key pressed
// -> search product on OpenFoodFact
$('#search').click(function() {getOpenFoodFactProductInfo();});
$('#barcode').keypress
(
    function(event)
    {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13')
        {
            getOpenFoodFactProductInfo();
        }
        event.stopPropagation();
    }
);