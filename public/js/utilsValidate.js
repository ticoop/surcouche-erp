// Fonction qui affiche un message d'erreur pour un élément donné
function invalidFeedback(element, message)
{
    $(element+'-error').html(message);
    $(element).addClass('is-invalid');
}

// Fonction qui nettoie les messages d'erreurs pour un element donné
function clearFeedback(element)
{
    $(element+'-error').html();
    $(element).removeClass('is-invalid');
}


// Cette fonction applique un timeout de 5sec sur les messages de succès
setTimeout
(
    function()
    {
        $('div.success').fadeOut();
    },
    5000
);