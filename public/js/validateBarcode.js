// Vérifie que le code barre est au bon format (EAN13)
// et affiche des messages d'erreurs le cas échéant
function validateBarcode()
{
    // $(element).val() return a string -> We have to check that it's an Integer
    barcode = $('#barcode').val()
    // Then check that there isn't any other characters thant [0-9]
    if (!(/^([0-9]\d*)$/.test(barcode)))
    {
        invalidFeedback('#barcode', 'Saisissez un nombre (vérifiez les espaces en fin de chaîne');
        setInput();
        return false
    }

    // It's an Integer, but an EAN13 ?
    if (barcode.length < 13 || barcode.length > 13)
    {
        invalidFeedback('#barcode', 'Longueur code barre invalide : ' + barcode.length + ' au lieu de 13 chiffres');
        setInput();
        return false
    }

    // All good, we remove eventually feedback
    clearFeedback('#barcode');
    return true
}