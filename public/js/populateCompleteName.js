function populateCompleteName()
{
    label = $('#name').val();
    label = label.toUpperCase();

    completeName = label;


    brand = $('#brand').val();
    brand = brand.toUpperCase();
    
    if (brand != '')
        completeName += ' ' + brand;


    weight = $('#weight').val();
    weightType = $('#weightType option:selected').val();
    if (weightType == 'weight')
        weightUnitList = ['g', 'Kg'];
    else
        weightUnitList = ['ml', 'L'];

    weightUnit = weightUnitList[0];
    
    if ((weight != 0) && (weight != ''))
    {
        if (weight >= 1000)
        {
            weight = weight / 1000;
            weightUnit = weightUnitList[1];
        }

        weightFinal = '(' + weight + weightUnit + ')';
        weightFinal.replace('.', ',');

        completeName += ' ' + weightFinal;
    }

    
    if ($('#bio').prop('checked'))
        completeName += ' BIO'
    
    $('#completeName').val(completeName);
}

$('#name, #brand, #weightType, #weight, #bio').change
(
    function()
    {
        populateCompleteName();
    }
);