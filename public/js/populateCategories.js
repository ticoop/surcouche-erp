// Populate Categories ->
// Clear options in category and subCategory
// Choose if we populate with scale or not products
// (depends on #barcodeType value)
function populateCategories(toScale)
{
    clearOptions('#category');
    clearOptions('#subCategory');

    if (toScale)
    {
        filename=filenameProductToScale;
    } else
    {
        filename=filenameProductNotToScale;
    }

    readTextFile(file=filename, callback=function(text)
    {
        var data = JSON.parse(text); //parse JSON

        addOption($('#category'), '', '');
        for (cat in data)
        {
            addOption($('#category'), data[cat].key, data[cat].name);
        }
    });
}

// Each time value of #barcodeType change
// -> re-populate categories
$('#barcodeType').change(
    function()
    {
        if ($(this).val() == 'to-scale')
        {
            populateCategories(toScale=true);
        }
        else
        {
            populateCategories(toScale=false);
        }
    }
);


// Populate the categories on load page
populateCategories(toScale=false);