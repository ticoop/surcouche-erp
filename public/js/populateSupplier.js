function populateSupplier()
{
    $.ajax
    ({
        type: "GET",
        url: "getSupplierList"
    })
    .done
    (
        function( supplierList )
        {
            addOption($('#supplier'), '', '');
            for (supplier in supplierList)
                addOption($('#supplier'), supplierList[supplier].id, supplierList[supplier].name);
        }
    );
}

$('#supplier').change
(
    function()
    {
        optionSelected = $('#supplier option:selected');
        idNewProduct = $(optionSelected).attr('newProduct');
        if ((idNewProduct == undefined) && ($(optionSelected).val != ''))
        {
            
        }
    }
);

populateSupplier();