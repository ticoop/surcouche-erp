// Populate the select input #accountancyCodeSell
function populateAccountancySell()
{
    readTextFile(file='public/Ressources/accountancyCodeSell.json', callback=function(text)
    {
        var data = JSON.parse(text);
        addOption($('#accountancyCodeSell'), '', '')
        for (acc in data)
        {
            addOption
            (
                $('#accountancyCodeSell'),
                data[acc].accCode,
                data[acc].accCode + ' - ' + data[acc].accName
            );
        }
    });
}

// Populate the accountancy code sell on load page
// After just change which one is selected
// (Never re-populate)
populateAccountancySell();


// Each time value of #subCategories change
// -> change the Accountancy Code Sell
$('#subCategory').change
(
    function()
    {
        $('#accountancyCodeSell option:selected').prop('selected', false);
        accCode = $('#subCategory option:selected').parent().attr('code');
        $('#accountancyCodeSell option[value=' + accCode + ']').prop('selected', true);
    }
);