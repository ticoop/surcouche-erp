// File resources : list of categories and subCategories
// 2 kind : to scale or not
filenameProductToScale="public/Ressources/productsToScale.json"
filenameProductNotToScale="public/Ressources/productsNotToScale.json"

// Read a JSON file and send to a callback function to define
function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

// Clear options and optgroup in a SelectField
// + unselect option for Accountancy Code Sell
// + Remove prefix of Reference
function clearOptions(elementSelector)
{
    $(elementSelector + ' option').remove();
    $(elementSelector + ' optgroup').remove();

    $(elementSelector).trigger('change');
}

// Add an option to a select element
function addOption(element, value, name)
{
    $(element).append('<option value="' + value + '">' + name + '</option>');
}

// Add a optgroup to a select element
// with Accountancy Code Sell as code attr
// and refPrefix as attr too
function addGroup(element, code, name, refPrefix)
{
    $(element).append
    (
        '<optgroup code="' +
        code +
        '" refPrefix="' +
        refPrefix +
        '" label="' +
        name +
        '"></optgroup>'
    );
}
