// Populate subCategories
// clear option of #subCategory
// Choose if we populate with scale or not products
// (depends on #barcodeType value)
// Find which category is currently selected
// Create group for each section (Accountancy Code Sell)
// And add option for each section
function populateSubCategories(toScale)
{
    clearOptions('#subCategory');

    if (toScale)
    {
        filename=filenameProductToScale;
    } else
    {
        filename=filenameProductNotToScale;
    }

    readTextFile(file=filename, callback=function(text)
    {
        var data = JSON.parse(text); //parse JSON

        catKeySelected = $('#category option:selected').val();
        catIdx = 0;
        for (cat in data)
        {
            if (catKeySelected == data[cat].key)
            {
                catIdx = cat;
                break;
            }
                
        }

        addOption($('#subCategory'), '', '');
        for (section in data[catIdx].sectionList)
        {
            addGroup(
                $('#subCategory'),
                data[catIdx].sectionList[section].accCode,
                data[catIdx].sectionList[section].accName,
                data[catIdx].sectionList[section].refPrefix
            );

            for (subCat in data[catIdx].sectionList[section].subCatList)
            {
                addOption
                (
                    $('#subCategory optgroup[label="'+data[catIdx].sectionList[section].accName+'"]'),
                    data[catIdx].sectionList[section].subCatList[subCat].key,
                    data[catIdx].sectionList[section].subCatList[subCat].name
                );
            }
        }
    });
}


// Each time value of #category change
// -> re-populate subCategories
$('#category').change(
    function()
    {
        if ($('#barcodeType').val() == 'to-scale')
        {
            populateSubCategories(toScale=true);
        }
        else
        {
            populateSubCategories(toScale=false);
        }
    }
);