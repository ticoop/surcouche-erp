// Cette fonction va définir un trigger sur tous les bouttons d'édition de champ
// Au clic sur le boutton, le champ s'active ou se désactive
// Utilisé pour les champs désactivés par défaut
$('button[edit]').click(
    function()
    {
        element = $(this).attr('edit');

        if ($('#'+element).prop('disabled'))
        {
            $('#' +element).prop('disabled', false);
            $(this).removeClass('btn-warning');
            $(this).addClass('btn-outline-warning');
        } else
        {
            $('#' +element).prop('disabled', true);
            $(this).removeClass('btn-outline-warning');
            $(this).addClass('btn-warning');
        }
    }
)