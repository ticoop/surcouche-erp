function putANumberOn3Characters(num)
{
    res = num;
    if (num < 10)
        res = '00' + num;

    else if (num < 100)
        res = '0' + num;

    return res;
}


function ean13from12digit(numStr)
{
    checkDigit = 0;

    digits = numStr.split('').map(Number);
    odd = [];
    even = [];
    for (let i = 0; i < digits.length; i++) {
        // tricky : index start at 0 for array
        // but at 1 to determinated if a number is even or not
        // so -> (i+1)
        if ((i+1) % 2 === 0) {
          // number is even
          even.push(digits[i]);
        } else {
          // number is odd
          odd.push(digits[i]);
        }
    }

    // Sum all the digits in even positions and multiply by 3;
    for (digit in even)
        checkDigit += even[digit];

    checkDigit *= 3;


    // Add all the digits in odd positions (except for the last one which is check digit) to the number you've got;
    for (digit in odd)
        checkDigit += odd[digit]
    
    // Divide that number by 10 and take the reminder;
    checkDigit = checkDigit % 10;
    
    // If the reminder is not 0, subtract it from 10.
    if (checkDigit != 0)
        checkDigit = 10 - checkDigit;

    // Job done!
    return numStr + checkDigit;
}


// Ce trigger va modifier le comportement du champ code-barre
// en fonction de la valeur choisie :
// barcodeType à 3 valeurs possibles :
// - Code-barre existant
// - Code-barre inexistant ou Produit à peser

// Si le code-barre est existant, on active l'édition du champ code-barre
// et on permet la recherche dans OpenFoodFact

// À l'inverse, si le code-barre est inexistant ou le produit est à peser
// le code barre répond alors à des règles Ti Coop.
// Dans ce cas, on désactive par défaut l'édition du champ
// et on remplace le boutton de recherche par un bouton d'édition du champ
$('#barcodeType').change(
    function()
    {   if ($('#barcodeType').val() == 'existing')
        {
            $('#barcode').prop('disabled', false);
            $('#search').show();
            $('#edit-barcode').hide();

        } else
        {
            $('#barcode').prop('disabled', true);
            $('#search').hide();
            $('#edit-barcode').show();

            $('#barcode').val(null);
            setInput();
            clearFeedback('#barcode');

            if ($('#barcodeType').val() == 'to-scale')
            {
                $('#barcode').val(2100000000000);
                $('#weight').val(1000);
            } else
            {
                $('#barcode').val(2000000000000);
            }
        }
    }
);


function populateRefAndBarcode()
{
    prefix = $('#subCategory option:selected').parent().attr('refPrefix');
    if (prefix == undefined)
    {
        $('#ref').val('');
        return
    }

    idSupplier = $('#supplier option:selected').val();
    if ((idSupplier == undefined) || (idSupplier == ''))
    {
        $('#ref').val('');
        return
    }
        
    else
        idSupplier = putANumberOn3Characters(idSupplier);


    $.ajax
    ({
        type: "GET",
        url: "getIdNewProduct/" + prefix + '-' + idSupplier + '-'
    })
    .done
    (
        function( idNewProduct )
        {
            ref = prefix + '-' + idSupplier + '-' + idNewProduct;
            $('#ref').val(ref);

            if ($('#barcodeType') != 'existing')
            {
                barcodePrefix = $('#barcode').val().substring(0,2);
                barcode = barcodePrefix + idSupplier + idNewProduct + "0000";
                $('#barcode').val(ean13from12digit(barcode));
            }
        }
    );
}

// Each time value of #subCategories change Reference
$('#subCategory, #supplier').change
(
    function()
    {
        populateRefAndBarcode();
    }
);
