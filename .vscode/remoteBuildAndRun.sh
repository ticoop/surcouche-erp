#!/bin/bash

REMOTE_SERVER='ticoop-prod'
REMOTE_DIR='/data/docker/app/surcouche-erp/'
PROJECT_NAME='surcouche-erp'

echo "Rm remote dir"
ssh $REMOTE_SERVER rm -r $REMOTE_DIR

echo "Start scp to remote server"
rsync -av -e ssh --exclude='env' --exclude='__pycache__' ./* $REMOTE_SERVER:$REMOTE_DIR

echo "docker remove container and image"
ssh $REMOTE_SERVER docker stop $PROJECT_NAME
ssh $REMOTE_SERVER docker container rm  $PROJECT_NAME
ssh $REMOTE_SERVER docker image rm $PROJECT_NAME:latest

echo "docker build"
ssh $REMOTE_SERVER docker build -t $PROJECT_NAME $REMOTE_DIR

echo "docker run"
ssh $REMOTE_SERVER docker run -d -p 127.0.0.1:5001:5000/tcp --name $PROJECT_NAME $PROJECT_NAME:latest