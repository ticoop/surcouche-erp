##########
# IMPORT #
##########

# Basic Flask
from flask import Flask
from flask import redirect          # For redirect URI
from flask import url_for           # For get an URL for a resources
from flask import render_template   # For Jinja templates

# Authentification
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import check_password_hash

# Forms
from forms import addProductForm
from forms import importProductForm
from flask import flash                     # For display flash messages
from werkzeug.utils import secure_filename  # For uploads files
import constants                            # For CSRF secret key
import os                                   # For file path

# ERP
from erp import Dolibarr    # For have the correct ERP
from flask import jsonify   # For put result in JSON format

# Logging
import logging


############
# INIT APP #
############

app = Flask(__name__, static_url_path='/public', static_folder='./public')

# Get Conf
if app.config["ENV"] == "production":
    app.config.from_object("config.ProductionConfig")
else:
    app.config.from_object("config.DevelopmentConfig")

# Init Authentification
auth = HTTPBasicAuth()
users = app.config['USERS']
roles = app.config['ROLES']

@auth.get_user_roles
def get_user_roles(username):
    return roles.get(username)

@auth.verify_password
def verify_password(username, password):
    if username in users and \
            check_password_hash(users.get(username), password):
        return username

# Init ERP
if app.config['ERP_APP'] not in constants.ERP_AVAILABLE_APP:
    print(error)
elif app.config['ERP_APP'] == 'Dolibarr':
    erp = Dolibarr(app.config["ERP_BASEURL"], app.config["ERP_APIKEY"])

# Init CSRF
app.secret_key = constants.SECRET_KEY

logging.basicConfig(filename = 'filename.log', level=logging.DEBUG, format = f'%(levelname)s : %(message)s')

##########
# ROUTES #
##########

@app.route('/')
@auth.login_required
def index():
    return render_template('home.html.jinja', roles=get_user_roles(auth.current_user()))

@app.route('/updateProductPrice/<product_label>/<product_barcode>', methods=['GET'])
@auth.login_required(role='updateProductPrice')
def update_product_price(product_label, product_barcode):
    app.logger.debug(product_label)
    app.logger.debug(product_barcode)

    return ""

@app.route('/addProduct', methods=['GET', 'POST'])
@auth.login_required(role='addProduct')
def addProduct():
    form = addProductForm()
    # form.barcode.data = 3068110702235
    # form.barcode.data = 0123456789012
    # form.barcode.data = 2100000000000 

    if form.validate_on_submit():
        # To test
        image = form.image.data
        filename = secure_filename(image.filename)
        image.save(os.path.join(
            app.instance_path, 'images', filename
        ))

        flash('Produit ajouté : ' + form.name.data, 'success alert-success')
        return redirect(url_for('addProduct'))
    
    elif form.is_submitted():
        flash('Erreur lors de la création du produit ', 'error alert-danger alert-dismissible fade show')

    return render_template('addProduct.html.jinja', form=form)

# Call in Ajax by /addProduct
@app.route('/getSupplierList')
@auth.login_required(role='addProduct')
def getSupplierList():
    return jsonify(erp.getSupplierList())

# Call in Ajax by /addProduct
@app.route('/getIdNewProduct/<filter>')
@auth.login_required(role='addProduct')
def getIdNewProduct(filter):
    return jsonify(erp.getIdNewProduct(filter))



@app.route('/updateProduct', methods=['GET', 'POST'])
@auth.login_required(role='updateProduct')
def updateProduct():
    form = importProductForm()

    if form.validate_on_submit():
        # To test
        csvFile = form.csvFile.data
        filename = secure_filename(csvFile.filename)
        csvFile.save(os.path.join(
            app.instance_path, 'files', filename
        ))

        flash('Produits mis à jour', 'success alert-success')
        return redirect(url_for('index'))
    
    elif form.is_submitted():
        flash('Erreur lors de la mise à jour des produits ', 'error alert-danger alert-dismissible fade show')

    return render_template('updateProductFromCSV.html.jinja', form=form)




##############
# LAUNCH APP #
##############
if __name__ == '__main__':
    app.run()